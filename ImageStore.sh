#!/bin/bash
set -e
GITLAB_REGISTRY=gitlab.scsuk.net:5005
echo "Login GITLAB_REGISTRY($GITLAB_REGISTRY)"
docker login "$GITLAB_REGISTRY"
echo '##################################################################'
echo "Pulling external images and storing them locally at $GITLAB_REGISTRY"
echo '------------------------------------'

GITLAB_REGISTRY_PROJECT="$GITLAB_REGISTRY"/scs-systems/ext_registry
grep -v ^# ./ImageStore.conf | while read DETAIL ; do
   SOURCE=$(echo "$DETAIL" | cut -d '#' -f 1)
   REPO=$(echo "$DETAIL" | cut -d '#' -f 2)
   EXTERNAL_TAG=$(echo "$DETAIL" | cut -d '#' -f 3)
   INTERNAL_TAG=$(echo "$DETAIL" | cut -d '#' -f 4)
   INTERNAL_TAG=${INTERNAL_TAG:-$EXTERNAL_TAG} # if blank will be same as external tag
   EXTERNAL_REPO="$SOURCE/$REPO"
   INTERNAL_REPO="$GITLAB_REGISTRY_PROJECT/$REPO"
   EXTERNAL_IMAGE="$EXTERNAL_REPO:$EXTERNAL_TAG"
   INTERNAL_IMAGE="$INTERNAL_REPO:$INTERNAL_TAG"
   echo "PULLING: $EXTERNAL_IMAGE"
   set -x
   docker pull "$EXTERNAL_IMAGE"
   IMAGE_ID=$(docker image ls "$REPO" -q)
   echo "ADD TAG: $INTERNAL_IMAGE"
   set -x
   docker tag $IMAGE_ID "$INTERNAL_IMAGE"
   set +x
   echo "PUSH TO: $INTERNAL_IMAGE"
   set -x
   docker push "$INTERNAL_IMAGE"
   set +x
   echo "Clear down images - tidy up"
   set -x
   docker image remove "$REPO:$EXTERNAL_TAG"
   docker image remove "$INTERNAL_IMAGE"
   set +x
   echo '------------------------------------'
done 

echo "See: https://gitlab.scsuk.net/scs-systems/phase-2/scs_registry/container_registry"
echo '##################################################################'
